db.users.find({age:{$lt:76}})
db.users.find({age:{$lte:76}})

db.users.find({age:{$ne:76}})


db.users.find({$or: [{firstName:"Neil"},{firstName:"Bill"}]})

db.users.find({$or: [{firstName:"Neil"},{age:{$gt:30}}]})

db.users.find({$and:[{age:{$ne:82}},{age:{$ne:76}}]})


// Field Protection

//Inclusion
db.users.find(
	{
		firstName:"Jane"
	},
	{
		firstName:1,
		lastName:1,
		"contact.phone":1
	}

)

//Exclusion

db.users.find(
	{
		firstName:"Jane"
	},
	{
		contact:0,
		department:0,
		
	}

)

// Supressing the ID field

db.users.find(
	{
		firstName:"Jane"
	},
	{
		firstName:1,
		lastName:1,
		contact:1,
		_id:0
		
	}

)



//Evaluation of Query operators

//$regex operator

//Case Sensitive
db.users.find({firstName:{$regex:'N'}})


//Case Insensitive
db.users.find({firstName:{$regex:'N',$options: '$i'}})









